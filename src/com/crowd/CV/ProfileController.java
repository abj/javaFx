/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crowd.CV;

import com.crowd.Util.Singleton;
import com.crowd.animations.FadeInLeftTransition;
import com.crowd.entities.Membre;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import org.kairos.components.MaterialButton;

/**
 * FXML Controller class
 *
 * @author ASR1
 */
public class ProfileController implements Initializable {
    @FXML
    private AnchorPane paneParent;
    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private DatePicker date_naissance;
    @FXML
    private TextField username;
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField password1;
    @FXML
    private PasswordField password2;
    
    private Membre membre;
    LoginController l1;
    @FXML
    private MaterialButton btnAppliquerModif;
    @FXML
    private MaterialButton btnmodifier;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

     this.fadeIn();
        this.startUp();
    }

    private void startUp() {
    //    nom.setText(membre.getUsername());
        username.setDisable(true);
        nom.setDisable(true);
        prenom.setDisable(true);
        email.setDisable(true);
        password.setDisable(true);
        password1.setDisable(true);
        password2.setDisable(true);
        date_naissance.setDisable(true);
        setNom(Singleton.getInstance().getMembre().getNom());
        setPrenom(Singleton.getInstance().getMembre().getPrenom());
        username.setText(Singleton.getInstance().getMembre().getUsername());
        email.setText(Singleton.getInstance().getMembre().getEmail());
        
       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
LocalDate date = LocalDate.parse(Singleton.getInstance().getMembre().getDate_naissance(), formatter);
        System.out.println(date.toString());
date_naissance.setValue(date);


    }
   
                    
    
     private void fadeIn() {
        new FadeInLeftTransition(paneParent).play();
    }


    @FXML
    private void searchImage(ActionEvent event) {
    }
/*
    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }
*/

    public TextField getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom.setText(nom);
    }

    public TextField getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom.setText(prenom);
    }

    public DatePicker getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(DatePicker date_naissance) {
        this.date_naissance = date_naissance;
    }

    public TextField getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username.setText(username);
    }

    public TextField getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email.setText(email);
    }

    public PasswordField getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password.setText(password);
    }

    @FXML
    private void btnAppliquerModif(ActionEvent event) {
    }

    @FXML
    private void btnmodifier(ActionEvent event) {
        
        username.setDisable(false);
        nom.setDisable(false);
        prenom.setDisable(false);
        email.setDisable(false);
        password.setDisable(false);
        password1.setDisable(false);
        password2.setDisable(false);
        date_naissance.setDisable(false);
        System.out.println(Singleton.getInstance().getMembre().getId_membre()+"------");
    }


    
    
     
}